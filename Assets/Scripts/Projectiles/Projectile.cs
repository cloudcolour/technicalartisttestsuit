﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public float Propulsion;
    public ParticleSystem ExplosionParticles;
    public Transform BulletVisual;
    public float DestroyTime = 2.0f;

    protected Rigidbody rigid;
    protected BoxCollider collideBox;
    protected float spawnTime = 0.0f;

    protected virtual void Awake()
    {
        collideBox = transform.GetComponent<BoxCollider>();
        rigid = transform.GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        spawnTime -= Time.deltaTime;
    }

    public bool IsDestroy()
    {
        return spawnTime <= 0;
    }

    public void Init()
    {
        BulletVisual.gameObject.SetActive(true);
        if (rigid != null)
            rigid.AddForce(transform.forward * Propulsion, ForceMode.Impulse);
        spawnTime = DestroyTime;
    }

    public virtual void Explode()
    {
        if (collideBox != null)
            collideBox.enabled = false;
        BulletVisual.gameObject.SetActive(false);

        if (rigid != null)
        {
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;
        }

        ExplosionParticles.Play();

        //Extended destroy time
        ParticleSystem.MainModule mainModule = ExplosionParticles.main;
        spawnTime = mainModule.duration;
    }

    void OnCollisionEnter(Collision collision)
    {
        if ((BulletVisual != null) && !BulletVisual.gameObject.activeInHierarchy)
            return;

        Explode();
    }

    public void Reset()
    {
        if (rigid != null)
        {
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;
        }
        if (collideBox != null)
            collideBox.enabled = true;
    }
}
