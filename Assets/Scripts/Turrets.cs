﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrets : MonoBehaviour
{
    [Range(0, 360)] public int FiringArc;
    public float CooldownBetweenShot;
    public LayerMask ProjectileLayer;

    protected float currentCooldownBetweenShot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentCooldownBetweenShot -= Time.deltaTime;

        if (currentCooldownBetweenShot <= 0)
            Shot();
    }

    protected virtual void Shot()
    {
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y += Random.Range(-FiringArc / 2, FiringArc / 2);
        Quaternion shotAngle = Quaternion.Euler(rotationVector);
        Projectile projectile = ProjectileManager.Inst.CreateBullet(transform.position, shotAngle, ProjectileLayer);
        currentCooldownBetweenShot = CooldownBetweenShot;
    }
}
