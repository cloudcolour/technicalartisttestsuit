﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    static public ProjectileManager Inst;

    public Projectile ProjectileObject;

    private List<Projectile> projectilePools;

    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        projectilePools = new List<Projectile>();
        PrepareAPoolBullets();
    }

    private void Update()
    {
        foreach (var projectile in projectilePools)
        {
            if (!projectile.gameObject.activeInHierarchy)
                continue;
            if (projectile.IsDestroy())
            {
                projectile.Reset();
                projectile.gameObject.SetActive(false);
            }
        }
    }

    void PrepareAPoolBullets()
    {
        for (int i = 0; i < 20; i++)
        {
            Projectile obj = Instantiate (ProjectileObject,this.transform).GetComponent<Projectile>();
            obj.gameObject.SetActive(false);
            projectilePools.Add(obj);
        }
    }

    public Projectile CreateBullet(Vector3 position, Quaternion angle,LayerMask layer)
    {
        Projectile tmp;
        tmp = GetProjectileFromPool();
        tmp.transform.position = position;
        tmp.transform.rotation = angle;
        tmp.gameObject.layer = Mathf.RoundToInt(Mathf.Log(layer.value, 2));
        tmp.gameObject.SetActive(true);
        tmp.Init();
        return tmp;
    }

    private Projectile GetProjectileFromPool()
    {
        foreach (var p in projectilePools)
        {
            if (!p.gameObject.activeInHierarchy)
                return p;
        }
        Projectile obj = Instantiate(ProjectileObject, this.transform).GetComponent<Projectile>();
        obj.transform.parent = this.transform;
        projectilePools.Add(obj);
        return obj;
    }
}